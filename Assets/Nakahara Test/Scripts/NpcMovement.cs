﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.AI;
using UnityEngine.UI;

[RequireComponent(typeof(NavMeshAgent))]
[RequireComponent(typeof(DestinationController))]
public class NpcMovement : MonoBehaviour
{
    [SerializeField] private int WaitSeconds = 10;

    private NavMeshAgent navAgent = null;
    [SerializeField] private DestinationController destinationController;

    [SerializeField] private GameObject MsgPanel = null;
    [SerializeField] private Text Message = null;
    [SerializeField] private Text CntDown = null;

    private bool isPause = false;
    private float DefSpeed;

    [SerializeField] private int counter = 0;
    private bool inCoroutine = false;


    void Start()
    {
        navAgent = GetComponent<NavMeshAgent>();
        destinationController = GetComponent<DestinationController>();
        navAgent.SetDestination(destinationController.GetDestination());

        DefSpeed = navAgent.speed;
        MsgPanel.gameObject.SetActive(false);

 
        Message.text = "自動巡回を開始します。\n(click or tap)";
        CntDown.text = "";
        MsgPanel.gameObject.SetActive(true);
        isPause = true;

    }

    void Update()
    {
        if (isPause)
        {
            if (Input.GetButtonDown("Fire1"))
            {
                navAgent.speed = DefSpeed;
                navAgent.updateRotation = true;

                destinationController.CreateDestination();
                navAgent.SetDestination(destinationController.GetDestination());

                MsgPanel.gameObject.SetActive(false);

                isPause = false;
            }

            if (!inCoroutine)
            {
                StartCoroutine(GoNext());
            }
        }
        else
        {
            if (Vector3.Distance(transform.position, destinationController.GetDestination()) < 3f)
            {
                navAgent.speed = 0f;
                navAgent.updateRotation = false;
                isPause = true;

                switch (destinationController.GetOrder())
                {
                    case 0:
                        Message.text = "ここはキッチンです。\n冷蔵庫、レンジ、電子レンジ、食洗機があります。";
                        break;
                    case 1:
                        Message.text = "ここはリビングルームです。\n液晶テレビがあります。";
                        break;
                    case 2:
                        Message.text = "ここはベッドルームです。\nIKEAのクイーンサイズのベッドがあります。";
                        break;
                    default:
                        Message.text = "ここはどこかわかりません。";
                        break;
                }

                 Message.text += ("\n\n(click or tap)");

                MsgPanel.gameObject.SetActive(true);
            }

        }
    }

    IEnumerator GoNext()
    {
        inCoroutine = true;
        counter = WaitSeconds;

        while (counter > 0)
        {
            yield return new WaitForSeconds(1.0f);
            counter--;
            CntDown.text = counter.ToString() + "秒後に自動的に次に進みます。";
        }

        destinationController.CreateDestination();
        navAgent.SetDestination(destinationController.GetDestination());

        navAgent.speed = DefSpeed;
        navAgent.updateRotation = true;

        MsgPanel.gameObject.SetActive(false);
        CntDown.text = "";

        isPause = false;

        inCoroutine = false;
        
    }

}