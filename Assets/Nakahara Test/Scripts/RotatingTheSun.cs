﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class RotatingTheSun : MonoBehaviour
{

    public float rotateSpeed = 0.1f;
    public Vector3 rot = new Vector3(270f, 330f, 0f);

    void Start()
    {
        // transform.localRotation = Quaternion.Euler(rot);

    }

    // Update is called once per frame
    void Update()
    {
        //　徐々に回転させる、X軸を反対方向に回転
        // transform.Rotate(-Vector3.right * rotateSpeed * Time.deltaTime);

        rot.x += rotateSpeed * Time.deltaTime;
        transform.localRotation = Quaternion.Euler(rot);
    }
}
