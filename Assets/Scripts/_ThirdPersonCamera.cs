﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class _ThirdPersonCamera : MonoBehaviour
{
    public float cameraSpeed = 10f;
    public Vector3 cameraAngle;
    public GameObject targetObject;
    public Vector3 targetOffset;
    public float cameraDistance;
    public float pitchMax = 70f;

    [Tooltip("衝突判定を行うレイヤー")]
    public LayerMask LayerMask;
    public float MinDistance = 1f;
    public float MaxDistance = 10f;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        cameraAngle.x -= Input.GetAxis("Mouse Y") * cameraSpeed;
        if (cameraAngle.x < -pitchMax) cameraAngle.x = -pitchMax;
        else if (cameraAngle.x > pitchMax) cameraAngle.x = pitchMax;
        cameraAngle.y += Input.GetAxis("Mouse X") * cameraSpeed;
        SetFov();

        Vector3 posOrigin = targetObject.transform.position + targetOffset;
        Vector3 offsetFromOrigin = Quaternion.Euler(cameraAngle) * new Vector3(0, 0, -1f) * cameraDistance;
        Ray ray = new Ray(posOrigin, offsetFromOrigin);
        RaycastHit hitInfo;
        if (Physics.Raycast(ray, out hitInfo, cameraDistance, LayerMask))
        {
            float hitDistance = (hitInfo.point - posOrigin).magnitude;
            if (hitDistance < MinDistance) hitDistance = MinDistance;
            //transform.position = hitInfo.point - offsetFromOrigin.normalized * (distance <= MinDistance ? MinDistance : 0.1f);
            SetPosition(posOrigin + offsetFromOrigin.normalized * (hitDistance - 0.1f));
        }
        else
        {
            SetPosition(posOrigin + offsetFromOrigin);
        }

        //transform.position = posOrigin + targetOffset + Quaternion.Euler(cameraAngle) * new Vector3(0,0,-1f) * cameraDistance;
        SetRotation(Quaternion.Euler(cameraAngle));
    }

    public void SetFov(float f = 0)
    {
        cameraDistance -= Input.GetAxis("Mouse ScrollWheel") - f;
        if (cameraDistance < MinDistance)       cameraDistance = MinDistance;
        else if (cameraDistance > MaxDistance)  cameraDistance = MaxDistance;
    }

    void SetPosition(Vector3 newPos)
    {
        transform.position = Vector3.Lerp(transform.position, newPos, 0.5f);
    }
    void SetRotation(Quaternion newRot)
    {
        transform.rotation = Quaternion.Lerp(transform.rotation, newRot, 0.5f);
    }
}
