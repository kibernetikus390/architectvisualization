﻿/*
    ようこそ画面のカメラを制御します
 */
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UI_WelcomeCameraControll : MonoBehaviour
{
    public GameObject cam;
    public float rotateSpeed = 30f;
    public float idleRotateSpeed = 10f;

    bool inputted;

    void Update()
    {
        if (!inputted)
        {
            Turn(idleRotateSpeed);
        }

        //Debug.Log("Hor:"+ Input.GetAxis("Horizontal"));
        if (Input.GetAxis("Horizontal") != 0)
        {
            Turn(Input.GetAxis("Horizontal") * -rotateSpeed);
            inputted = true;
        }
    }

    public void TurnLeft()
    {
        Turn(rotateSpeed);
        inputted = true;
    }

    public void TurnRight()
    {
        Turn(-rotateSpeed);
        inputted = true;
    }
    public void Turn(float rot)
    {
        cam.transform.rotation = Quaternion.Lerp(cam.transform.rotation, cam.transform.rotation * Quaternion.AngleAxis(rot, Vector3.up), Time.deltaTime);
    }
}
