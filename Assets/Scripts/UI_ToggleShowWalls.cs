﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI_ToggleShowWalls : MonoBehaviour
{
    public Camera cam;
    public LayerMask layerShowAll;
    public LayerMask layerHidden;
    public string ChangeInputName = "FreeCam";

    public Toggle toggleObject;
    private bool useHidden;
    private bool useInput;
    private bool waitKeyUp;

    // Start is called before the first frame update
    void Start()
    {
        cam.cullingMask = layerShowAll;
        useHidden = false;

        if (ChangeInputName != null && ChangeInputName != "") useInput = true;
    }

    void Update()
    {
        //Debug.Log(toggleObject.isOn);
        if (useInput)
        {
            if (waitKeyUp)
            {
                if (!Input.GetButton(ChangeInputName))
                {
                    waitKeyUp = false;
                }
            }
            else
            {
                if (Input.GetButton(ChangeInputName))
                {
                    toggleObject.isOn = !toggleObject.isOn;
                    waitKeyUp = true;
                }
            }
        }
    }

    public void Change()
    {
        useHidden = !toggleObject.isOn;
        cam.cullingMask = useHidden ? layerHidden : layerShowAll;
    }
}
