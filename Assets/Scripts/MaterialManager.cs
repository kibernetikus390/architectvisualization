﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MaterialManager : MonoBehaviour
{
    public GameObject[] wallObjects;
    public Material[] materials;
    public int matIndex;

    public GameObject[] floorObjects;
    public Material[] floorMats;
    public int floorIndex;

    public GameObject[] ceilingObjects;
    public int ceilingIndex;

    private bool waitToRelease;

    void Start()
    {
        ChangeMat();
        ChangeMatFloor();
        ChangeMatCeiling();
    }

    void Update()
    {
        if (!waitToRelease)
        {
            if (Input.GetKey(KeyCode.LeftArrow))
            {
                ChangeMat(-1);
                waitToRelease = true;
            }
            else if (Input.GetKey(KeyCode.RightArrow))
            {
                ChangeMat(1);
                waitToRelease = true;
            }
            else if (Input.GetKey(KeyCode.DownArrow))
            {
                ChangeMatFloor(1);
                waitToRelease = true;
            }
            else if (Input.GetKey(KeyCode.UpArrow))
            {
                ChangeMatCeiling(1);
                waitToRelease = true;
            }
        }
        else
        {
            if (!Input.GetKey(KeyCode.LeftArrow) && !Input.GetKey(KeyCode.RightArrow) && !Input.GetKey(KeyCode.DownArrow) && !Input.GetKey(KeyCode.UpArrow))
            {
                waitToRelease = false;
            }
        }
    }

    void ChangeMat(int deltaIndex = 0) {
        matIndex += deltaIndex;
        matIndex = (matIndex < 0) ? materials.Length - 1 : (matIndex >= materials.Length) ? 0 : matIndex;

        foreach (GameObject obj in wallObjects)
        {
            obj.GetComponent<Renderer>().material = materials[matIndex];
        }
    }

    void ChangeMatFloor(int deltaIndex = 0)
    {
        floorIndex += deltaIndex;
        floorIndex = (floorIndex < 0) ? floorMats.Length - 1 : (floorIndex >= floorMats.Length) ? 0 : floorIndex;

        foreach (GameObject obj in floorObjects)
        {
            obj.GetComponent<Renderer>().material = floorMats[floorIndex];
        }
    }
    void ChangeMatCeiling(int deltaIndex = 0)
    {
        ceilingIndex += deltaIndex;
        ceilingIndex = (ceilingIndex < 0) ? floorMats.Length - 1 : (ceilingIndex >= floorMats.Length) ? 0 : ceilingIndex;

        foreach (GameObject obj in ceilingObjects)
        {
            obj.GetComponent<Renderer>().material = materials[ceilingIndex];
        }
    }
}
