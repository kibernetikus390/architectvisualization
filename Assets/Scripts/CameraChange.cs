﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class CameraChange : MonoBehaviour
{
    [Header("カメラのリスト")]
    public Camera[] cameras;
    public int index = 0;

    [Header("自由移動カメラ")]
    public GameObject FreeCamCharacter;
    public Camera FreeCam;
    private bool useFreeCam;

    private bool waitForRelease;
    private bool waitForFreeCamRelease;

    void Start()
    {
        DisableAllCameras();
        //UpdateCameraStates();
    }

    void Update()
    {
        /*
            カメラを変更する
         */
        //◆フリーカメラ
        if (!waitForFreeCamRelease)
        {
            if (Input.GetButton("FreeCam"))
            {
                EnableFreeCamera();
            }
        }
        else
        {
            //キーが離されるのを待つ
            if (!Input.GetButton("FreeCam")) waitForFreeCamRelease = false;
        }

        //◆固定カメラ
        if (!useFreeCam)
        {
            if (!waitForRelease)
            {
                if (Input.GetAxis("Horizontal") > 0)
                {
                    NextCamera();
                }
                else if (Input.GetAxis("Horizontal") < 0)
                {
                    PrevCamera();
                }
            }
            else
            {
                //キーが離されるのを待つ
                if (Input.GetAxis("Horizontal") == 0) waitForRelease = false;
            }
        }
    }
    public void StartFromFreeCamera()
    {
        /*
            フリーカメラから開始する
            出現位置を変更しない
         */
        useFreeCam = true;
        UpdateCameraStates();
    }
    public void EnableFreeCamera()
    {
        /*
            フリーカメラの有効・無効を切り替えrる
         */
        waitForFreeCamRelease = true;
        useFreeCam = !useFreeCam;
        if (useFreeCam)
        {
            NavMeshHit hit;
            if (NavMesh.SamplePosition(cameras[index].gameObject.transform.position, out hit, 10f, NavMesh.AllAreas))
            {
                FreeCamCharacter.transform.position = hit.position;
                FreeCam.transform.position = hit.position;
                FreeCam.GetComponent<_ThirdPersonCamera>().cameraAngle = cameras[index].gameObject.transform.rotation.eulerAngles;
            }
        }
        UpdateCameraStates();
    }
    public void DisableFreeCamera()
    {
        /*
         * フリーカメラを無効にする
         */
        useFreeCam = false;
        UpdateCameraStates();
    }
    public void PrevCamera()
    {
        /*
            前のカメラへ
         */
        Debug.Log("prevcamera");
        waitForRelease = true;
        index = (index - 1);
        if (index < 0) index = cameras.Length - 1;
        UpdateCameraStates();
    }

    public void NextCamera()
    {
        /*
            次のカメラへ
         */
        Debug.Log("nextcamera");
        waitForRelease = true;
        index = (index + 1);
        if (index >= cameras.Length) index = 0;
        UpdateCameraStates();
    }
    void DisableAllCameras()
    {
        FreeCam.gameObject.SetActive(false);
        FreeCamCharacter.SetActive(false);
        foreach (Camera cam in cameras)
        {
            cam.gameObject.SetActive(false);
        }
    }
    void UpdateCameraStates()
    {
        /*
            カメラをアップデート
         */
        if (useFreeCam)
        {
            FreeCam.gameObject.SetActive(true);
            FreeCamCharacter.SetActive(true);
            foreach (Camera cam in cameras)
            {
                cam.gameObject.SetActive(false);
            }
        }
        else
        {
            FreeCam.gameObject.SetActive(false);
            FreeCamCharacter.SetActive(false);
            for (int i = 0; i < cameras.Length; i++)
            {
                if (i == index)
                {
                    cameras[i].gameObject.SetActive(true);
                    continue;
                }
                cameras[i].gameObject.SetActive(false);
            }
        }
    }
    public void StartFromIndex(int i)
    {
        index = i;
        useFreeCam = false;
        UpdateCameraStates();
    }
    public bool IsFreeCameraEnabled()
    {
        return useFreeCam;
    }
    public GameObject GetActiveCamera()
    {
        /*
            現在有効なカメラのGameObjectを取得
         */
        return cameras[index].gameObject;
    }
    public Camera GetActiveCameraComponent()
    {
        /*
            現在有効なカメラを取得
         */
        return cameras[index];
    }
    public Camera[] GetCameraList()
    {
        /*
            カメラリストを取得
         */
        return cameras;
    }

    public GameObject GetFreeCamCharacter()
    {
        return FreeCamCharacter;
    }
    public GameObject GetFreeCam()
    {
        return FreeCam.gameObject;
    }
}
