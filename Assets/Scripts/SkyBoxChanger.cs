﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkyBoxChanger : MonoBehaviour
{
    //Skyボックスマテリアル
    public Material[] materials;
    public int index = 0;
    //入力軸の名前
    public string InputAxisName = "ChangeSkybox";
    //方向性ライトの参照、色
    public Color[] dirLightColor;
    public Light dirLightObj;

    private bool waitReleased;

    // Start is called before the first frame update
    void Start()
    {
        UpdateSkybox();
    }

    // Update is called once per frame
    void Update()
    {
        if (waitReleased)
        {
            if (Input.GetAxis(InputAxisName) == 0f)
            {
                waitReleased = false;
            }
        }
        else
        {
            if (Input.GetAxis(InputAxisName) > 0f)
            {
                NextSkyBox();
            }
            else if (Input.GetAxis(InputAxisName) < 0f)
            {
                index--;
                if (index < 0) index = materials.Length - 1;
                UpdateSkybox();
            }
        }
    }

    public void NextSkyBox()
    {
        index++;
        if (index >= materials.Length) index = 0;
        UpdateSkybox();
    }
    public void PrevSkyBox()
    {
        index--;
        if (index < 0) index = materials.Length - 1;
        UpdateSkybox();
    }

    void UpdateSkybox()
    {
        waitReleased = true;

        //方向性ライトの色を変更
        dirLightObj.color = dirLightColor[index];

        //Skyboxを変更
        RenderSettings.skybox = materials[index];
        DynamicGI.UpdateEnvironment();
    }
}
