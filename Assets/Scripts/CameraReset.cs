﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CameraReset : MonoBehaviour
{
    private CameraChange camManager;
    private Camera[] cameras;
    private List<Quaternion> rots;
    private List<float> fovs;

    private Vector3 FreeCamCharacter_pos;
    private Quaternion FreeCamCharacter_rot;
    private Vector3 FreeCam_pos;
    private Quaternion FreeCam_rot;

    void Start()
    {
        rots = new List<Quaternion>();
        fovs = new List<float>();

        camManager = GetComponent<CameraChange>();
        cameras = camManager.GetCameraList();

        foreach (Camera cam in cameras)
        {
            rots.Add(cam.gameObject.transform.rotation);
            fovs.Add(cam.fieldOfView);
        }

        FreeCamCharacter_pos = camManager.GetFreeCamCharacter().transform.position;
        FreeCamCharacter_rot = camManager.GetFreeCamCharacter().transform.rotation;
        FreeCam_pos = camManager.GetFreeCam().transform.position;
        FreeCam_rot = camManager.GetFreeCam().transform.rotation;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetButton("Cancel"))
        {
            ResetCameras();
        }
    }
    public void ResetCameras()
    {
        SceneManager.LoadScene(0);
    }
    //public void ResetCameras()
    //{
    //    for (int i = 0; i < cameras.Length; i++)
    //    {
    //        cameras[i].gameObject.transform.rotation = rots[i];
    //        cameras[i].fieldOfView = fovs[i];
    //    }

    //    camManager.GetFreeCamCharacter().transform.position = FreeCamCharacter_pos;
    //    camManager.GetFreeCamCharacter().transform.rotation = FreeCamCharacter_rot;
    //    camManager.GetFreeCam().transform.position = FreeCam_pos;
    //    camManager.GetFreeCam().transform.rotation = FreeCam_rot;
    //}
}
