﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraZoom : MonoBehaviour
{
    public float speed;
    public float fovMax = 120f;
    public float fovMin = 20f;
    public _ThirdPersonCamera freeCam;
    private CameraChange camManager;

    // Start is called before the first frame update
    void Start()
    {
        camManager = GetComponent<CameraChange>();
    }

    // Update is called once per frame Input.GetAxis("Horizontal")
    void Update()
    {
        SetZoom();
    }

    void SetZoom(float f = 0)
    {
        if (camManager.IsFreeCameraEnabled())
        {
            freeCam.SetFov(f/10f);
            return;
        }
        float newFov = camManager.GetActiveCameraComponent().fieldOfView - Input.GetAxis("Mouse ScrollWheel") * speed + f;
        if (newFov < fovMin) newFov = fovMin;
        if (newFov > fovMax) newFov = fovMax;
        camManager.GetActiveCameraComponent().fieldOfView = newFov;
    }

    public void ZoomIn() {
        SetZoom(-10f);
    }
    public void ZoomOut()
    {
        SetZoom(10f);
    }
}
