﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UI_HideUI : MonoBehaviour
{
    public GameObject target;
    private bool waitToRelease;

    // Update is called once per frame
    void Update()
    {
        if (!waitToRelease)
        {
            if (Input.GetKey(KeyCode.Tab))
            {
                target.SetActive(!target.activeSelf);
                waitToRelease = true;
            }
        }
        else
        {
            if (!Input.GetKey(KeyCode.Tab))
            {
                waitToRelease = false;
            }
        } 
    }
}
