﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnityChanCharacterController : MonoBehaviour
{
    public Camera myCamera;

    private Animator MyAnimator;
    private CharacterController MyController;

    [Tooltip("落下速度")]
    public float gravity = 18;
    [Tooltip("走る速度")]
    public float runningSpeed = 5;
    [Tooltip("ジャンプの上昇速度")]
    public float jumpSpeed = 8.5f;

    private Vector3 MoveDirection = Vector3.zero;

    // Start is called before the first frame update
    void Start()
    {
        MyAnimator = GetComponent<Animator>();
        MyController = GetComponent<CharacterController>();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        //◆ 移動操作
        Vector3 newMovement = Vector3.zero;
        //入力を受け取る
        newMovement.x = Input.GetAxis("Horizontal");
        newMovement.z = Input.GetAxis("Vertical");
        //移動速度で正則化
        newMovement = newMovement.normalized * runningSpeed;
        //入力ベクトルにカメラのクォータニオンを適用
        newMovement = Quaternion.Euler(0, myCamera.gameObject.transform.rotation.eulerAngles.y, 0) * newMovement;
        //移動を補完　スライディング中はきびきびと回転できないようにする
        newMovement = Vector3.Lerp(MoveDirection, newMovement, Time.deltaTime * 5f);

        //◆ 重力を加算
        if (MyController.isGrounded)
        {
            MoveDirection.y = -.5f;
        }
        else
        {
            MoveDirection.y -= gravity * Time.deltaTime;
        }

        //◆ 移動を適用
        MoveDirection.x = newMovement.x;
        MoveDirection.z = newMovement.z;
        MyController.Move(MoveDirection * Time.deltaTime);

        //◆ 回転を適用
        bool movekey = Input.GetAxis("Horizontal") != 0 || Input.GetAxis("Vertical") != 0;
        if (movekey)
        {
            Quaternion targetRot = Quaternion.LookRotation(new Vector3(MoveDirection.x, 0, MoveDirection.z));
            Quaternion currentRot = Quaternion.LookRotation(new Vector3(transform.forward.x, 0, transform.forward.z));
            transform.rotation = Quaternion.Lerp(transform.rotation, targetRot, Time.deltaTime * 10);

            //アニメーションブレンド用に 入力方向と現在の向きのY回転の差(-180~180)を取得
            float angleBetween = Quaternion.Angle(targetRot, currentRot);
            if ((transform.eulerAngles.y - currentRot.eulerAngles.y) < 0) angleBetween *= -1;
            MyAnimator.SetFloat("Direction", Mathf.Clamp(Mathf.Lerp(MyAnimator.GetFloat("Direction"), angleBetween / 30f, Time.deltaTime * 3), -1f, 1f));
        }
        else
        {
            MyAnimator.SetFloat("Direction", Mathf.Lerp(MyAnimator.GetFloat("Direction"), 0, Time.deltaTime * 3));
        }

        //◆ メカニムのパラメータを設定
        MyAnimator.SetFloat("Speed", new Vector3(MoveDirection.x, 0, MoveDirection.z).magnitude);
        //MyAnimator.SetBool("run", movekey);             //走っているか？
        //MyAnimator.SetFloat("moveY", MoveDirection.y);  //Y速度
        //MyAnimator.SetInteger("jumpCount", jumpMax - JumpRemaining);    //ジャンプ回数
        //MyAnimator.SetBool("onGround", OnMovingPlatform ? true : MyController.isGrounded);        //接地しているか？ 移動プラットフォームに乗ってる場合は、Controllerに関わらず真を渡す

    }
}
