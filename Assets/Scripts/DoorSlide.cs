﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorSlide : MonoBehaviour
{
    public Vector3 targetOffset;
    public float speed = 1f;

    private Vector3 targetPos;
    private Vector3 defaultPos;

    // Start is called before the first frame update
    void Start()
    {
        targetPos = defaultPos = transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        transform.position = Vector3.Lerp(transform.position, targetPos, Time.deltaTime * speed);
    }
    public void Open()
    {
        targetPos = defaultPos + targetOffset;
    }
    public void Close()
    {
        targetPos = defaultPos;
    }
}
