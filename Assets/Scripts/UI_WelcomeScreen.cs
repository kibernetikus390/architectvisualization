﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UI_WelcomeScreen : MonoBehaviour
{
    public GameObject welcomePanel;
    public GameObject iconPanel;
    public GameObject welcomeCamera;

    public GameObject[] enableOnStart;

    // Start is called before the first frame update
    void Start()
    {
        welcomeCamera.SetActive(true);
        welcomePanel.SetActive(true);
        iconPanel.SetActive(false);

        foreach (GameObject obj in enableOnStart)
        {
            obj.SetActive(false);
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void StartTour()
    {
        /*
            内覧を始めるボタンを押し、カメラ選択画面に映る
         */
        welcomePanel.SetActive(false);
        iconPanel.SetActive(true);

    }
    public void DisableWelcomePanels()
    {
        /*
            （↑からカメラを選択し、）Canvasを切り替える
         */
        welcomeCamera.SetActive(false);
        welcomePanel.SetActive(false);
        iconPanel.SetActive(false);

        foreach (GameObject obj in enableOnStart)
        {
            obj.SetActive(true);
        }
    }
}
