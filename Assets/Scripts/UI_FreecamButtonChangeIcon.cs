﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UI_FreecamButtonChangeIcon : MonoBehaviour
{
    public CameraChange camManager;
    public GameObject enableWhenFreecamAvailable;
    public GameObject disableWhenFreecamAvailable;

    // Update is called once per frame
    void Update()
    {
        if (camManager.IsFreeCameraEnabled())
        {
            enableWhenFreecamAvailable.SetActive(true);
            disableWhenFreecamAvailable.SetActive(false);
        }
        else
        {
            enableWhenFreecamAvailable.SetActive(false);
            disableWhenFreecamAvailable.SetActive(true);
        }
    }
}
