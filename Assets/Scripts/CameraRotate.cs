﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraRotate : MonoBehaviour
{
    public float speed;
    public bool invertY = true;
    private CameraChange camManager;
    public float maxPitchAbs = 80f;

    void Start()
    {
        camManager = GetComponent<CameraChange>();
    }

    // Update is called once per frame
    void Update()
    {
        //Vector3 input = new Vector3(Input.GetAxis("Mouse Y") * (invertY ? -1f : 1f), Input.GetAxis("Mouse X"), 0);
        //camManager.GetActiveCamera().transform.rotation *= Quaternion.Euler(input);
        if (camManager.IsFreeCameraEnabled()) return;
        Vector3 camEuler = camManager.GetActiveCamera().transform.rotation.eulerAngles;
        //インプットを加算
        camEuler = new Vector3(camEuler.x + Input.GetAxis("Mouse Y") * (invertY ? -1f : 1f) * speed, camEuler.y + Input.GetAxis("Mouse X") * speed, camEuler.z);
        //ピッチを制限
        camEuler.x = (camEuler.x + 180) % 360 - 180;
        if (camEuler.x > maxPitchAbs) camEuler.x = maxPitchAbs;
        else if (camEuler.x < -maxPitchAbs) camEuler.x = -maxPitchAbs;
        //セット
        camManager.GetActiveCamera().transform.rotation = Quaternion.Euler(camEuler);
    }
}
