﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UI_CameraIcon : MonoBehaviour
{
    public Camera cam;
    public GameObject target;

    RectTransform rectTransform;

    // Start is called before the first frame update
    void Start()
    {
        rectTransform = GetComponent<RectTransform>();
    }

    // Update is called once per frame
    void Update()
    {
       rectTransform.position = cam.WorldToScreenPoint(target.transform.position);
    }
}
