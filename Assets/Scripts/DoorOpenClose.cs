﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorOpenClose : MonoBehaviour
{
    public float speed = 1f;
    public float yawOpen;

    private Quaternion defaultRot;
    private Quaternion targetRot;

    // Start is called before the first frame update
    void Start()
    {
        targetRot = defaultRot = transform.rotation;
    }

    // Update is called once per frame
    void Update()
    {
        transform.rotation = Quaternion.Lerp(transform.rotation, targetRot, Time.deltaTime * speed);
    }

    public void Open()
    {
        targetRot = defaultRot * Quaternion.AngleAxis(yawOpen, Vector3.up);
    }
    public void Close()
    {
        targetRot = defaultRot;
    }
}
