﻿/*
    ボタンを”押しっぱなしの間”イベントを発火し続けます
 */
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UI_ButtonHold : MonoBehaviour
{
    public GameObject eventHandler;
    public string eventName;

    RectTransform myRect;

    // Start is called before the first frame update
    void Start()
    {
        myRect = GetComponent<RectTransform>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButton(0))
        {
            //Debug.Log("MousePosition" + Input.mousePosition);
            //Debug.Log("RectPosition " + myRect.position);
            //Debug.Log("RectSizeDelta" + myRect.sizeDelta);

            if (    myRect.position.x - myRect.sizeDelta.x / 2f <= Input.mousePosition.x && 
                    Input.mousePosition.x <= myRect.position.x + myRect.sizeDelta.x / 2f &&
                    myRect.position.y - myRect.sizeDelta.y / 2f <= Input.mousePosition.y && 
                    Input.mousePosition.y <= myRect.position.y + myRect.sizeDelta.y / 2f)
            {
                //Debug.Log("HOLD");
                eventHandler.SendMessage(eventName, SendMessageOptions.DontRequireReceiver);
            }
        }
    }
}
