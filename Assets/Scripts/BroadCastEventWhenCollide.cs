﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BroadCastEventWhenCollide : MonoBehaviour
{
    public GameObject targetObject;
    public GameObject eventHandler;
    public string eventName;

    void OnCollisionEnter(Collision other)
    {
        Fire(other.gameObject);
    }
    void OnTriggerEnter(Collider other)
    {
        Fire(other.gameObject);
    }

    void Fire(GameObject obj)
    {
        if (obj == targetObject)
        {
            eventHandler.BroadcastMessage(eventName, SendMessageOptions.DontRequireReceiver);
        }
    }
}
