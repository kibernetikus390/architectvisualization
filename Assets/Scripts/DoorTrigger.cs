﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorTrigger : MonoBehaviour
{
    void OnTriggerEnter(Collider other)
    {
        BroadcastMessage("Open", SendMessageOptions.DontRequireReceiver);
    }
    void OnTriggerExit(Collider other)
    {
        BroadcastMessage("Close", SendMessageOptions.DontRequireReceiver);
    }
}
